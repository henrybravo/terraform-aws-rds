#         _                           _                            _
# _ __ __| |___       _ __   ___  ___| |_ __ _ _ __ ___  ___  __ _| |
#| '__/ _` / __|_____| '_ \ / _ \/ __| __/ _` | '__/ _ \/ __|/ _` | |
#| | | (_| \__ \_____| |_) | (_) \__ \ || (_| | | |  __/\__ \ (_| | |
#|_|  \__,_|___/     | .__/ \___/|___/\__\__, |_|  \___||___/\__, |_|
#                    |_|                 |___/                  |_|

provider "aws" {
  region = "eu-west-1"
}

data "aws_vpc" "default" {
  #default = true
  id      = "vpc-fb022b9c"
}

data "aws_subnet_ids" "all" {
  vpc_id  = "${data.aws_vpc.default.id}"
}

data "aws_security_group" "default" {
  vpc_id  = "${data.aws_vpc.default.id}"
  name    = "postgres-rds"
}

module "db" {
  source                 = "../../"
  identifier             = "demodbpublic"
  engine                 = "postgres"
  engine_version         = "9.6.3"
  instance_class         = "db.t2.large"
  allocated_storage      = 5
  storage_encrypted      = false
  #kms_key_id            = "arm:aws:kms:<region>:<account id>:key/<kms key id>"
  name                   = "demodbpublic"
  # NOTE: Do NOT use 'user' as the value for 'username' as it throws:
  # "Error creating DB Instance: InvalidParameterValue: MasterUsername
  # user cannot be used as it is a reserved word used by the engine"
  username               = "demouser"
  password               = "mysecurepassword"
  port                   = "5432"
  vpc_security_group_ids = ["${data.aws_security_group.default.id}"]
  maintenance_window     = "Mon:00:00-Mon:03:00"
  backup_window          = "03:00-06:00"
  publicly_accessible    = "true"

  # disable backups to create DB faster
  backup_retention_period = 0

  tags = {
    Owner       = "user"
    Environment = "dev"
  }

  # DB subnet group
  subnet_ids = ["${data.aws_subnet_ids.all.ids}"]

  # DB parameter group
  family = "postgres9.6"

  # DB option group
  major_engine_version = "9.6"

  # Snapshot name upon DB deletion
  final_snapshot_identifier = "demodb"
}
