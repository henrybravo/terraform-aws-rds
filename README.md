# terraform-aws-rds

This is a fork from the official terraform's github [repository](https://github.com/terraform-aws-modules). This repository is also referenced from snippet ["deploy postgresql with terraform on AWS RDS"](https://gitlab.com/snippets/1747045). Instructions for use are detailed in there.

## AWS RDS Terraform module

Terraform module which creates RDS resources on AWS.

These types of resources are supported:

* [DB Instance](https://www.terraform.io/docs/providers/aws/r/db_instance.html)
* [DB Subnet Group](https://www.terraform.io/docs/providers/aws/r/db_subnet_group.html)
* [DB Parameter Group](https://www.terraform.io/docs/providers/aws/r/db_parameter_group.html)
* [DB Option Group](https://www.terraform.io/docs/providers/aws/r/db_option_group.html)

Root module calls these modules which can also be used separately to create independent resources:

* [db_instance](https://gitlab.com/henrybravo/terraform-aws-rds/tree/master/modules/db_instance) - creates RDS DB instance
* [db_subnet_group](https://gitlab.com/henrybravo/terraform-aws-rds/tree/master/modules/db_subnet_group) - creates RDS DB subnet group
* [db_parameter_group](https://gitlab.com/henrybravo/terraform-aws-rds/tree/master/modules/db_parameter_group) - creates RDS DB parameter gr
oup
* [db_option_group](https://gitlab.com/henrybravo/terraform-aws-rds/tree/master/modules/db_option_group) - creates RDS DB option group
